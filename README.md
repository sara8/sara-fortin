# Slate Plus

__Slate Plus__ is a theme starting point / boilerplate / build tool. Its goal is to provide a consistent base to all of our custom future projects.

## Features
- Bundles assets with [webpack]
- Transpiles JavaScript with [babel]
- Compiles SCSS files with [node-sass] and [sass-loader]
- CSS Vendor prefixing with [autoprefixer] and [postcss]
- Optimized bundles (JS / CSS minification)
- Watch mode with browser auto-reloading using [BrowserSync]
- Ability to detect theme script entry points, and conditionally load them (by generating a custom Liquid snippet)
- Bundles all third-party libraries into a single bundle (`vendors.js`)
- Bundles all CSS into a single CSS bundle (`theme.css`)
- Lints JS code using [ESLint]

## Included libraries
- [React] with some sample components
- [Jest] testing framework with some example tests
- [Enzyme] testing utility library for [React]
- [Bootstrap] CSS framework
- [Reactstrap] Bootstrap React components
- [React InstantSearch] Algolia React integration

## Custom Webpack plugins
- `ThemeKitPlugin`: Adds support for using [Shopify Theme Kit]
- `ScriptTagsSnippetPlugin`: Generates a custom Liquid snippet called `script-tags` which is able to collect and organize all scripts found in `src/assets/scripts` folder
- `BrowserSyncPlugin`: Starts a BrowserSync server and reloads the browser after every change
- `TransformSectionsPlugin`: Enables use of [webpack] bundled libraries and modern JavaScript features in Shopify sections

# Installation
## Prerequisites
The following software should be installed on your machine:
- [NodeJS]
- [NPM] or [Yarn]
- [Shopify Theme Kit]

## Configuration
- Create a `config.yml` file in the root directory. Details can be found on the [Shopify Theme Kit] page.
- Create an `.env` file in the root directory. It currently supports the following settings:
  - `WATCH_URL` (__REQUIRED__): the url of the shop's preview page you are currently developing. Select "preview" on your theme, press the 'Share preview' button at the bottom, copy the url and paste it here.
  - `DEPLOY` (__OPTIONAL__): Whether to upload to Shopify servers after building. __Defaults to__ `true`
  - Example:
    ```
    WATCH_URL=<your generated link>
    DEPLOY=false
    ```

# Commands
- `npm run build` or `yarn build`: creates an optimized bundle into the `dist` folder and if `DEPLOY` is `true` uploads it to Shopify using [Shopify Theme Kit].
- `npm start` or `yarn start`: Starts watch mode. Rebuilds only what is necessary and starts [Shopify Theme Kit] in watch mode. Starts a [BrowserSync] server and reloads the browser after changes.
- `npm run upload` or `yarn upload`: Same as `theme deploy`
- `npm run test` or `yarn test`: Runs [Jest] test bundle
- `npm run sync` or `yarn sync`: Helps us with the reconciliation process. Downloads the theme files into a `remote` folder (wiped before each command invocation), which we can diff our files in `src` with. I recommend diffing the individual sub folders selectively (e.g. `remote/themes` with `src/themes`, etc).

# Files and folders
- `src`: contains the source code
- `utils`: contains custom Webpack plugins
- `.babelrc`: [babel] configuration
- `.browserslistrc`: [autoprefixer] configuration, which allows us to specify supported browsers
- `.editorconfig`: [Editorconfig] file, to have a consistent text editor setting (indentation, line endings, etc.)
- `.eslintrc.js`: [ESLint] configuration file
- `jest.config.js`: [Jest] configuration file
- `postcss.config.js`: [postcss] configuration
- `test.setup.js`: [Enzyme] setup script, initializes the needed [React] adapter
- `webpack.config.js`: [webpack] configuration (maybe the most important of all)

# How do I...?
## ...add a new script?
The first thing you have to ask yourself is where you want to use the script.
- if you are on a _template_ (e.g. you're editing a standard Shopify template in the `src/templates` folder), you have to put the script into the `src/assets/scripts/templates` folder, and give it the same name (_without_ the `.liquid` extension) as the _template_ you want to include the script in.

  Example: `product.liquid` -> `product.js` (or if you are using JSX / React in the file, stick to the convention and name it `product.jsx`).

- if you are on a _section_ (e.g. you're editing a `liquid` file in `src/sections`), you have to put the script into the `src/assets/scripts/sections` folder, and give it the same name (_without_ the `.liquid` extension) as the _section_ you want to include the script in.

  Example: `header.liquid` -> `header.js` (or if you are using JSX / React in the file, stick to the convention and name it `header.jsx`).

  Keep in mind that sections work differently than regular scripts. All section scripts are collected by Shopify and merged into a separate file, and included in the `<head>` section of the page. This means that section scripts are __not__ included in the main app bundle.

  There are also _lifecycle_ events related to section scripts, which is an advanced topic. Further details can be read at [Shopify sections documentation].

  __Disclaimer__: because of sections working differently, this method of script inclusion in sections is considered _experimental_.

- if you are in a _layout_ (very rarely we need them), then the procedure is the same as for templates.

  Right now it only has a `theme.liquid`, which contains an scss import. This is to tell webpack to process the stylesheets. Technically you can import third party libraries here, and it will be bundled into the _vendor_ bundle.

  Most of the time you don't have to worry about it, however.

- if you are in a _snippet_, check the place where you want to use it. If it's a _template_, see template instructions. If it's a _section_, see section instructions.

## ...reuse code?
Since the scripts are Javascript _modules_, you can use `import`s to reuse code. The way you organize these reusable modules is _up to you_.

If you look at the `src/assets/scripts/templates` folder, there are other sub-folders in it, each of them containing related code (e.g `components` folder contains React components, `utils` folder contains useful utility functions, etc). This is an example of code organization.

To reuse / import these bits of code, mark the function / class / object with an `export` clause. Then, use an `import` statement at the top of another file:

```js
// src/assets/scripts/templates/utils/index.js
export function myFunction() {
  // do stuff
}

// src/assets/scripts/templates/product.jsx
import { myFunction } from './utils';

// now you can use the imported function
myFunction();
```

So if you are wondering:
- the subfolders in each of `src/assets/scripts` directories (`sections`, `templates`) are __only__ for code reuse. So, for example, `src/assets/scripts/sections/common` folder can contain exported stuff to be used in section scripts. You don't have to use it if you don't want.

## ...import / use a library (e.g. jQuery)?
Since the scripts are modules, you must `import` the stuff you want to use in them. But first, you have to install it with the package manager of your choice (`npm` or `yarn`). For example jQuery is already included, and if you want to use it in some module, you have to `import` it:

```js
import * as $ from 'jquery';
```

The first time webpack encounters this import, it will include the whole library in the _vendor_ bundle, and anywhere else you use jQuery with this import, it will be reused from the bundle; so you don't have to worry about that it 'bloats' the bundle if you are using it multiple times.

There are cases when a library _needs to be globally available_, e.g. it has to exist on the `window` object (although we try to avoid it if possible).
In that case, we can simply include a lib from a CDN.

# Technical details
- Using [babel] allows us to utilize the latest JS standards and language features without worrying about browser compatibility
- [autoprefixer] negates the need to manually provide fallback directives, keeping our stylesheets clean
- The separate third-party lib bundle (`vendor.js`) is only included in `theme.liquid` which allows the browser to download all common code only once and cache it
- Only one css bundle is generated, which contains all of the theme's CSS plus any third party CSS libraries (e.g [Bootstrap]). This further aids performance by eliminating multiple http requests and helps browser caching as well
- The generated `script-tags` snippet conditionally loads scripts specific to the current page, e.g. `blog.template.js` will only load on the blog page, `product.template.js` will only load on the product page, etc. If we add a new theme script, the plugin should pick it up and generate the necessary Liquid logic for it
- Webpack only compiles `js`, `jsx`, `scss` files, `liquid` files and static assets are just copied unprocessed
- The `jsx` file extension is just a convention, they are normal JS files; basically if the script contains JSX expressions, its extension should be `jsx`
- Watch mode starts [webpack] in `development` mode, which means scripts and stylesheets will not be optimized; therefore it is important to use the `build` command (with `DEPLOY=true` which is the default) if we are publishing a production / publicly available theme
- `TransformSectionsPlugin` works by looking for a bundled/transpiled script with the same name as the section file in the `dist/assets` folder, wrapping it in `{% javascript %}` `{% endjavascript %}` tags and appending it to the section file.
- Tested under Linux and Windows, but should work on OSX as well

# Collection page Algolia widgets
![Components Mockup][algolia_components]
- <span style="color: #6334e3">CollectionsPage</span> (blue): This component wraps all other components
- <span style="color: #72bb53">CollectionsSidebar</span> (green): Contains the facets and filters (both Algolia-made and custom)
- <span style="color: #000000">CollectionsProducts</span> (black): Contains components related to displaying search results (sorting, hits, pagination...)
- <span style="color: #ff3823">CollectionsBar</span> (red): Query "fine-tuners" (sort, hits per page, etc.)
- <span style="color: #c238eb">Hits (Algolia-made)</span> (purple): Used to display search result
- <span style="color: #00c8f8">Pagination (Algolia-made)</span> (lightblue): You guessed it, pagination

Of course the above arrangement can be altered, it's just a sensible default.

## Our custom widgets
- `ProductHit`: This component is responsible for rendering a search result item. It's provided as a prop to the Algolia-made `Hits` component. This should be edited / styled according to the current project's design.

  props:
  - `hit`: the result object that comes from the Algolia index

  example:
  ```jsx
  <Hits hitComponent={ProductHit} />
  ```

- `CollapsiblePanel`: Allows wrapping widgets together and open/close them by clicking on its header.

  props:
  - `header`: The text to display as header
  - `children`: The wrapped components and DOM elements

  example:
  ```jsx
  <CollapsiblePanel header="Category">
    <CurrentRefinements />
    <HierarchicalMenu attributes={[
      'named_tags.cat',
      'named_tags.subcat'
    ]}/>
    <ClearRefinements />
  </CollapsiblePanel>
  ```

- `ResetableRefinementList`: Like a regular [RefinementList], but it includes an 'ALL' link, which clears this facet value from the query.

  props:
  - all props that the Algolia [RefinementList] accepts
  - `title` (optional): Display a title above the facet values

  example:
  ```jsx
  <ResetableRefinementList attribute="named_tags.colour" title="Colours" />
  ```

- `PrefixAwareRefinementList`: A regular [RefinementList] wrapped in with some additional logic: able to handle a facet value prefix.

  Useful for implementing "grouped" facet values (e.g. children/adult sizes). We can tag items with a prefix, and this component will chop off that prefix from the facet value.

  For example: a `size:Adult|M` tag will be displayed as `M`, but if clicked on it will refine with `Adult|M`, so only products tagged with `Adult|M` size will be returned from the query. The separator could be anything (even a space).

  props:
  - all props that the Algolia [RefinementList] accepts
  - `title` (optional): Display a title above the facet values
  - `prefix`: the prefix to chop off from the display (but keep in refinement)

  example:
  ```jsx
  <PrefixAwareRefinementList prefix="Baby" attribute="named_tags.size" />
  ```

# Cart related React components
Slate Plus includes a component infrastructure which uses [react-cart] under the hood. It's developed to be as easy to use and to be as easily customizable as possible.

### The components
- `CartButton`: Displays the number of items in the cart, which updates dynamically. Makes sense to _only_ use it on pages where AJAX cart operations are performed (typically `product`, `cart`). If you don't inject it on a page, the static Liquid based display will be shown (which is perfectly sufficient for other pages).

  example:
  ```jsx
  import React from 'react';
  import ReactDOM from 'react-dom';
  import CartButton from './components/cart/CartButton';

  ReactDOM.render(
    <CartButton />,
    document.getElementById('cart-btn')
  );
  ```

- `AddToCartButton`: a button (and quantity input) which allows putting the selected product variant into the cart. The variant selection is currently implemented with a combination of some Liquid & JS logic in the `product_options.liquid` section.

  props:
  - `product`: the product object. Currently it's read from Liquid and attached to the `window.slateplus` object (see `product.liquid`).

  example:
  ```jsx
  import React from 'react';
  import ReactDOM from 'react-dom';
  import AddToCartButton from './components/cart/AddToCartButton';

  ReactDOM.render(
    <AddToCartButton product={window.slateplus.activeItem} />,
    document.getElementById('add-to-cart-btn')
  );
  ```
- `CartItemContainer`: The base of a dynamic `cart` page. You only have to provide the components responsible for look & feel to it, and it should work.

  props:
  - `cart`: the cart object from [react-cart]
  - `headerComponent`: the component to render as the header (e.g. table headers, etc).
  - `itemComponent`: the component to render for a cart item
  - `footerComponent`: the component to render as the footer
  - `debounceTimer`: the amount of milliseconds to wait for the quantity inputs before sending the query request. Useful for reducing the number of ajax requests and avoiding bugs and inconsistencies.

  Attached component props:
  - `headerComponent` and `footerComponent`:
    - `cart`: the cart object from [react-cart]
  - `itemComponent`:
    - `item`: object representing an item in the cart. It's identical to elements in the `items` array of the [Shopify AJAX cart response].
    - `currency`: the currency abbreviation (e.g `AUD`), which can be used to render price values
    - `onChange(quantity: number)`: function which needs to be called on quantity change
    - `onRemove()`: function which needs to be called on remove

  example:
  ```js
  import React from 'react';
  import ReactDOM from 'react-dom';

  import { connectCart } from '../react-cart';
  import CartItemContainer from './components/cart/CartItemContainer';

  // example attached components
  import CartItem from './components/cart/CartItem';
  import CartPageHeader from './components/cart/CartPageHeader';
  import CartPageFooter from './components/cart/CartPageFooter';

  const ConnectedCartItemContainer = connectCart(CartItemContainer);

  ReactDOM.render(
    <ConnectedCartItemContainer
      headerComponent={CartPageHeader}
      itemComponent={CartItem}
      footerComponent={CartPageFooter}
      debounceTimer={1500}
    />,
    document.getElementById('cart-item-container')
  );
  ```
  example of an `itemComponent`:

  ```jsx
  import React from 'react';
  import PropTypes from 'prop-types';

  const CartItem = ({ item, currency, onChange, onRemove }) => {
    const doChange = e => onChange(e.target.value);
    const doRemove = () => onRemove();

    return (
      <div>
        <h5>{item.product_title}</h5>
        <img src={item.image} />
        <span>{item.price} {currency}</span>
        <input onChange={doChange} type="number" />
        <button onClick={doRemove}>REMOVE</button>
      </div>
    );
  };

  export default CartItem;
  ```

# Frequently Asked Questions
- ### Can I use Liquid in stylesheets?
  If you want to use liquid expressions in stylesheets, you have to put them in a `liquid` file (and of course include it in `theme.liquid`). There is a `theme.scss.liquid` file already (currently empty).

  These files will be processed on Shopify servers with their SCSS compiler, not our local one. It's worth mentioning that Shopify uses an SCSS compiler with a reduced feature set.

  Most of the time liquid expressions in stylesheets are used to integrate section settings in them (colours, font sizes, etc). There is an elegant and modern way to implement these, which is [CSS variables].

  Unfortunately, legacy browsers don't support it, but we're planning to develop a webpack plugin which will transform these variables into liquid expressions. When this plugin is done, it will be documented properly.

  For now if you want to use liquid in scss files, put these into `theme.scss.liquid` and include it after `theme.layout.css` in `theme.liquid`:
  ```
  {{ 'theme.scss.css' | asset_url | stylesheet_tag }}
  ```

- ### My code doesn't compile. What can I do?
  If webpack doesn't give you a clear error message that surely identifies the problem, you can try running the `lint` command:

  `npm run lint` or `yarn lint`

  This will run [ESLint] on all scripts. I also recommend installing an [ESLint] plugin for your IDE / text editor if it doesn't support it out of the box.

# Future plans
- Provide a base theme skeleton
- Add reusable React components
- Integrate (or implement) a GraphQL client
- Implement an AJAX cart system to replace CartJS
- Many more...

[webpack]: https://webpack.js.org
[babel]: https://babeljs.io
[node-sass]: https://github.com/sass/node-sass
[sass-loader]: https://github.com/webpack-contrib/sass-loader
[autoprefixer]: https://github.com/postcss/autoprefixer
[postcss]: https://github.com/postcss/postcss
[BrowserSync]: https://browsersync.io
[React]: https://reactjs.org
[Bootstrap]: https://getbootstrap.com
[Shopify Theme Kit]: https://shopify.github.io/themekit
[NodeJS]: https://nodejs.org
[NPM]: https://www.npmjs.com
[Yarn]: https://yarnpkg.com
[Editorconfig]: https://editorconfig.org
[ESLint]: https://eslint.org
[Jest]: https://jestjs.io
[Enzyme]: https://airbnb.io/enzyme
[Reactstrap]: https://reactstrap.github.io
[React InstantSearch]: https://community.algolia.com/react-instantsearch
[algolia_components]: ./docs/algolia_components.png
[RefinementList]: https://community.algolia.com/react-instantsearch/widgets/RefinementList.html
[Shopify sections documentation]: https://help.shopify.com/en/themes/development/sections#javascript-and-stylesheet-tags
[CSS Variables]: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables
[react-cart]: https://gitlab.com/createurs/react-cart
[Shopify AJAX cart response]: https://help.shopify.com/en/themes/development/getting-started/using-ajax-api#get-cart
