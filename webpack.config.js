const path = require('path');
const entries = require('./utils/entries');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ThemeKitPlugin = require('./utils/themekit');
const BrowserSyncPlugin = require('./utils/browsersync');
const ScriptTagsSnippetPlugin = require('./utils/generate-snippets');
const TransformSectionsPlugin = require('./utils/transform-section');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

require('dotenv').config();

const config = {
  stats: 'none',
  entry: entries,
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/assets')
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true
            }
          },
          {
            loader: 'eslint-loader'
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader'
        }]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      name: 'vendors'
    }
  },
  performance: {
    hints: false
  },
  plugins: [
    new FriendlyErrorsWebpackPlugin(),
    new CleanWebpackPlugin(['dist']),
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin([
      { from: './src/assets', to: path.resolve(__dirname, 'dist/assets'), ignore: ['*.js', '*.jsx', '*.scss', '.gitkeep'], flatten: true },
      { from: './src/config', to: path.resolve(__dirname, 'dist/config') },
      { from: './src/layout', to: path.resolve(__dirname, 'dist/layout') },
      { from: './src/locales', to: path.resolve(__dirname, 'dist/locales') },
      { from: './src/sections', to: path.resolve(__dirname, 'dist/sections') },
      { from: './src/snippets', to: path.resolve(__dirname, 'dist/snippets') },
      { from: './src/templates', to: path.resolve(__dirname, 'dist/templates') }
    ]),
    new ScriptTagsSnippetPlugin({ entries }),
    new TransformSectionsPlugin()
  ]
};

module.exports = argv => {
  const deploy = Object.is(process.env.DEPLOY, undefined) ? true : process.env.DEPLOY === 'true';

  if (argv.mode === 'development' && argv.watch) {
    const { WATCH_URL } = process.env;
    if (!WATCH_URL) {
      throw new Error('You have to provide a theme preview url during watch mode!');
    }

    config.plugins.push(new ThemeKitPlugin({
      cwd: __dirname,
      command: 'watch',
      args: ['-d', 'dist']
    }));

    config.plugins.push(new BrowserSyncPlugin(WATCH_URL));

  } else if (argv.mode === 'production' && deploy) {
    config.plugins.push(new ThemeKitPlugin({
      cwd: __dirname,
      command: 'deploy',
      args: ['-d', 'dist', '--nodelete']
    }));
  }

  // enable source maps during development
  if (argv.mode === 'development') {
    config.devtool = 'eval-source-map';
  }

  return config;
};
