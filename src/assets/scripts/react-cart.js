import { Container, Provider, Subscribe } from 'unstated';
import { createElement } from 'react';

function options(data) {
  return {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  };
}
function handleResponse(response) {
  return response.ok
    ? response.json()
    : response.json().then(r => Promise.reject(r));
}
class CartService {
  addItem(id, quantity) {
    return fetch('/cart/add.js', options({ quantity, id }))
      .then(() => this.getCart());
  }
  updateItem(id, quantity) {
    return this.updateOrRemove(id, quantity);
  }
  removeItem(id) {
    return this.updateOrRemove(id);
  }
  clear() {
    return fetch('/cart/clear.js', options(null)).then(handleResponse);
  }
  getCart() {
    return fetch('/cart.js').then(handleResponse);
  }
  updateOrRemove(id, quantity = 0) {
    return fetch('/cart/update.js', options({ updates: { [id]: quantity } }))
      .then(handleResponse);
  }
}

const defaultState = {
  status: 'normal',
  items: []
};
class CartContainer extends Container {
  constructor(cartService, state = defaultState) {
    super();
    this.cartService = cartService;
    this.state = state;
    this.getCart();
  }
  getCart() {
    return this.cartService.getCart()
      .then(response => this.setState(Object.assign({}, response, { status: 'normal' })))
      .catch(() => { });
  }
  addToCart(id, quantity) {
    return this.handle(this.cartService.addItem(id, quantity));
  }
  updateInCart(id, quantity) {
    return this.handle(this.cartService.updateItem(id, quantity));
  }
  removeFromCart(id) {
    return this.handle(this.cartService.removeItem(id));
  }
  clearCart() {
    return this.handle(this.cartService.clear());
  }
  handle(promise) {
    if (this.timeout) {
      clearInterval(this.timeout);
    }
    this.setState(state => (Object.assign({}, state, { status: 'loading' })));
    return promise
      .then(response => this.timeoutStatus('success', response))
      .catch(() => this.timeoutStatus('error'));
  }
  timeoutStatus(startingStatus, response) {
    if (response) {
      this.setState(Object.assign({}, response, { status: startingStatus }));
    }
    else {
      this.setState(state => (Object.assign({}, state, { status: startingStatus })));
    }
    this.timeout = setTimeout(() => {
      this.setState(state => (Object.assign({}, state, { status: 'normal' })));
      this.timeout = null;
    }, 3000);
  }
}

const cart = new CartContainer(new CartService());
function connectCart(Component) {
  return (props) => (createElement(Provider, { inject: [cart] },
    createElement(Subscribe, { to: [CartContainer] }, cart => createElement(Component, Object.assign({ cart: cart }, props)))));
}

export { connectCart };
