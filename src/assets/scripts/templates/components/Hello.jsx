import React from 'react';
import PropTypes from 'prop-types';

const Hello = props =>
  <h1>Hello from {props.compiler} and {props.framework}! This is the {props.page} page!</h1>;

Hello.propTypes = {
  compiler: PropTypes.string.isRequired,
  framework: PropTypes.string.isRequired,
  page: PropTypes.string.isRequired
};

export default Hello;
