import React from 'react';
import { ClearRefinements, HierarchicalMenu } from 'react-instantsearch-dom';

import ResetableRefinementList from '../algolia/ResetableRefinementList';
import PrefixAwareRefinementList from '../algolia/PrefixAwareRefinementList';
import CollapsiblePanel from '../algolia/CollapsiblePanel';

const sortAlphabetically = items => items.sort((a, b) => a.label.localeCompare(b.label));

const CollectionsSidebar = () =>
  <div className="sidebar">
    <h4>Shop By</h4>
    <CollapsiblePanel header="Category">
      <HierarchicalMenu attributes={[
        'named_tags.cat',
        'named_tags.subcat'
      ]}/>
    </CollapsiblePanel>
    <hr className="separator mt-3 mb-4"></hr>
    <h4>Filter By</h4>
    <CollapsiblePanel header="Size">
      <PrefixAwareRefinementList title="Baby" prefix="Baby" attribute="named_tags.size" transformItems={sortAlphabetically} />
      <PrefixAwareRefinementList title="Girl" prefix="Girl" attribute="named_tags.size" transformItems={sortAlphabetically} />
    </CollapsiblePanel>
    <CollapsiblePanel header="Colour">
      <ResetableRefinementList attribute="named_tags.color" transformItems={sortAlphabetically} />
    </CollapsiblePanel>
    <ClearRefinements />
  </div>;

export default CollectionsSidebar;
