import React from 'react';
import { Pagination } from 'react-instantsearch-dom';
import CollectionsBar from './CollectionsBar';
import ProductHit from './ProductHit';
import ProductHits from './ProductHits';

function CollectionsProducts() {
  return (
    <React.Fragment>
      <CollectionsBar />
      <ProductHits hitComponent={ProductHit} className="product-grid-3" />
      <div className="d-flex justify-content-center">
        <Pagination />
      </div>
    </React.Fragment>
  );
}

export default CollectionsProducts;
