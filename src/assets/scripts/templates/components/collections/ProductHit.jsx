import React from 'react';
import PropTypes from 'prop-types';
import Button from 'reactstrap/lib/Button';

import withHandlers from 'recompose/withHandlers';

const ProductHit = ({ hit, onClick }) =>
  <div className="product-hit text-center">
    <a href={`/products/${hit.handle}`}>
      <img src={hit.image} className="img-fluid" />
      <h6>{hit.title}</h6>
    </a>
    <p>
      ${hit.price}
    </p>
    <Button color="primary" onClick={onClick}>
      Add To Bag
    </Button>
  </div>;

ProductHit.propTypes = {
  hit: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

const enhance = withHandlers({
  onClick: ({ hit, onClick }) => () => onClick(hit)
});

export default enhance(ProductHit);
