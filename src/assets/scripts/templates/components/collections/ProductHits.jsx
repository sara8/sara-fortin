import React from 'react';
import PropTypes from 'prop-types';

import { connectHits } from 'react-instantsearch-dom';
import compose from 'recompose/compose';
import withProps from 'recompose/withProps';
import withStateHandlers from 'recompose/withStateHandlers';

import QuickView from '../QuickView';
import QuickViewModal from '../QuickViewModal';

const ProductHits = ({ hits, currentHit, open, toggle, className }) => (
  <React.Fragment>
    <QuickViewModal
      open={open}
      toggle={toggle}
      product={currentHit}
      component={QuickView}
    />
    <div className={className}>
      <ul className="ais-Hits-list">
        {hits}
      </ul>
    </div>
  </React.Fragment>
);

ProductHits.propTypes = {
  hits: PropTypes.arrayOf(PropTypes.object).isRequired,
  currentHit: PropTypes.object,
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired
};

const initialState = {
  currentHit: null,
  open: false
};

const stateUpdaters = {
  toggle: state => () => ({ ...state, open: !state.open }),
  onClick: () => currentHit => ({ currentHit, open: true })
};

const mapHits = ({ hits, hitComponent: HitComponent, onClick }) => ({
  hits: hits.map(hit =>
    <li key={hit.objectID} className="ais-Hits-item">
      <HitComponent hit={hit} onClick={onClick} />
    </li>
  )
});

const enhance = compose(
  connectHits,
  withStateHandlers(initialState, stateUpdaters),
  withProps(mapHits)
);

export default enhance(ProductHits);
