import React from 'react';
import { HitsPerPage, SortBy } from 'react-instantsearch-dom';
import { Button } from 'reactstrap';

function CollectionsBar() {
  return (
    <div className="d-flex align-items-center justify-content-between">
      <div>
        <label className="h6 mr-2">Sort by:</label>
        <SortBy
          className="d-inline-block"
          defaultRefinement="shopify_products_title_asc"
          items={[
            {
              label: 'Product title A-Z',
              value: 'shopify_products_title_asc'
            },
            {
              label: 'Product title Z-A',
              value: 'shopify_products_title_desc'
            },
            {
              label: 'Highest price',
              value: 'shopify_products_price_desc'
            },
            {
              label: 'Lowest price',
              value: 'shopify_products_price_asc'
            },
            {
              label: 'Oldest',
              value: 'shopify_products_date_asc'
            },
            {
              label: 'Newest',
              value: 'shopify_products_date_desc'
            }
          ]}
        />
      </div>

      <div className="d-flex">
        <Button outline color="secondary" className="d-inline-block mr-2">Show Products</Button>
        <HitsPerPage
          className="d-inline-block"
          defaultRefinement={12}
          items={[
            { value: 12, label: 'Show 12 hits' },
            { value: 24, label: 'Show 24 hits' },
            { value: 32, label: 'Show 32 hits' }
          ]}
        />
      </div>
    </div>
  );
}

export default CollectionsBar;
