import React from 'react';

import { Row, Col } from 'reactstrap';

import CollectionsSidebar from './CollectionsSidebar';
import CollectionsProducts from './CollectionsProducts';

const CollectionsPage = () =>
  <Row>
    <Col lg="3">
      <CollectionsSidebar />
    </Col>
    <Col>
      <CollectionsProducts />
    </Col>
  </Row>;

export default CollectionsPage;
