import React from 'react';
import PropTypes from 'prop-types';
import { RefinementList } from 'react-instantsearch-dom';
import { flow } from 'lodash-es';

const PrefixAwareRefinementList = ({ title, prefix, attribute, transformItems }) => {
  const transformLabel = label => label.substring(label.indexOf('|') + 1);
  const transform = items => items
    .filter(item => item.label.startsWith(prefix))
    .map(item => ({ ...item, label: transformLabel(item.label) }));

  const transformFn = transformItems ? flow([transformItems, transform]) : transform;

  return (
    <div>
      {title && <h6>{title}</h6>}
      <RefinementList attribute={attribute} transformItems={transformFn} />
    </div>
  );
};

PrefixAwareRefinementList.propTypes = {
  title: PropTypes.string,
  prefix: PropTypes.string.isRequired,
  attribute: PropTypes.string.isRequired,
  transformItems: PropTypes.func
};

export default PrefixAwareRefinementList;
