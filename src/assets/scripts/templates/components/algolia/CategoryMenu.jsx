import React from 'react';
import { connectRefinementList } from 'react-instantsearch-dom';
import { groupBy } from 'lodash-es';

const CategoryMenu = connectRefinementList(({ currentRefinement, items, refine }) => {
  const mapped = items.sort((a, b) => a.label.localeCompare(b.label)).map(item => {
    const split = item.label.split('|');
    return { ...item, label: split[1], category: split[0] };
  });

  const grouped = groupBy(mapped, 'category');
  const processed = Object.keys(grouped)
    .map(category => {
      const subcategories = grouped[category];

      return (
        <div key={category} className="refinementList">
          <span onClick={() => refine(`${category}|All`)}>
            {currentRefinement === `${category}|All` ? <b>{category}</b> : category}
          </span>
          <ul className="sub-refinementList">
            {subcategories.map(item => (
              <li key={item.label} onClick={() => refine(item.value)}>
                {item.isRefined ? <b>{item.label}</b> : item.label}
              </li>
            ))}
          </ul>
        </div >
      );
    });

  return (
    <React.Fragment>
      {processed}
    </React.Fragment>
  );
});

// const PrefixAwareHierarchicalMenu = props => <Connected {...props} />;

CategoryMenu.propTypes = {
};

CategoryMenu.defaultProps = {
};

export default CategoryMenu;
