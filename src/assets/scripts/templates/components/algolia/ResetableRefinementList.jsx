import React from 'react';
import { connectRefinementList } from 'react-instantsearch-dom';

const Connected = connectRefinementList(({ title, currentRefinement, items, refine }) => {

  return (
    <div className="refinementList">
      {title && <h5>{title}</h5>}
      <ul>
        <li onClick={() => refine('')}>{currentRefinement.length ? 'All' : <b>All</b>}</li>
        {items.map(item => (
          <li key={item.label} onClick={() => refine(item.value)}>{item.isRefined ? <b>{item.label}</b> : item.label}</li>
        ))}
      </ul>
    </div>
  );
});

const ResetableRefinementList = props => <Connected {...props} />;

export default ResetableRefinementList;
