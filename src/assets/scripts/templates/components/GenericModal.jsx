import React from 'react';
import PropTypes from 'prop-types';

import Modal from 'reactstrap/lib/Modal';
import ModalBody from 'reactstrap/lib/ModalBody';
import ModalHeader from 'reactstrap/lib/ModalHeader';

const GenericModal = ({ open, toggle, content }) => (
  <Modal isOpen={open} toggle={toggle}>
    <ModalHeader toggle={toggle}></ModalHeader>
    <ModalBody>{content}</ModalBody>
  </Modal>
);

GenericModal.propTypes = {
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  content: PropTypes.node
};

export default GenericModal;
