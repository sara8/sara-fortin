import React from 'react';
import PropTypes from 'prop-types';

import branch from 'recompose/branch';
import renderNothing from 'recompose/renderNothing';

import Button from 'reactstrap/lib/Button';

import { resizeImage } from 'templates/utils';

const QuickView = ({ product }) => (
  <div>
    <div>{product.title}</div>
    <img src={resizeImage(product.image, 'medium')} alt={product.title} />
    <Button>Add To Cart</Button>
  </div>
);

QuickView.propTypes = {
  product: PropTypes.object
};

const enhance =
  branch(({ product }) => Object.is(product, null), renderNothing);

export default enhance(QuickView);
