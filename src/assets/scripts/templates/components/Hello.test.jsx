import React from 'react';
import { shallow } from 'enzyme';
import Hello from './Hello';

test('Renders Hello', () => {
  const hello = shallow(<Hello compiler="Babel" framework="React" page="index" />);
  expect(hello.html()).toEqual('<h1>Hello from Babel and React! This is the index page!</h1>');
});
