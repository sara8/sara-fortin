import React from 'react';
import PropTypes from 'prop-types';

import mapProps from 'recompose/mapProps';

import GenericModal from './GenericModal';

const defaultComponent = props => (
  <pre>
    {JSON.stringify(props, null, 2).slice(0, 100)}
    ...
  </pre>
);

const propMapper = ({ open, toggle, product,
  component: Component = defaultComponent
}) => ({
  open,
  toggle,
  content: <Component product={product} />
});

const enhance = mapProps(propMapper);

const QuickViewModal = enhance(GenericModal);

QuickViewModal.propTypes = {
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  product: PropTypes.object,
  component: PropTypes.func.isRequired
};

export default QuickViewModal;
