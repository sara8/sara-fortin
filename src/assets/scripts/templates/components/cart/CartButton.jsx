import React from 'react';
import PropTypes from 'prop-types';
import { connectCart } from 'react-cart';

const CartButton = ({ cart }) =>
  <a className="btn" href="/cart">
    <i className="fas fa-shopping-cart"></i>
    <span className="ml-1">{ cart.state.item_count }</span>
  </a>;

CartButton.propTypes = {
  cart: PropTypes.object
};

export default connectCart(CartButton);
