import React from 'react';
import PropTypes from 'prop-types';

import { Button } from 'reactstrap';
import { find } from 'lodash-es';

import compose from 'recompose/compose';
import mapProps from 'recompose/mapProps';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { connectCart } from 'react-cart';

// ==================== Status aware button component ====================
const statusToColor = {
  loading: 'info',
  error: 'danger',
  success: 'success',
  normal: 'primary'
};

const statusToText = {
  loading: 'Adding...',
  error: 'Error!',
  success: 'Added To Bag!',
  normal: 'Add To Bag'
};

const statusEnhance = mapProps(({ status = 'normal', onClick }) => ({
  color: statusToColor[status],
  text: statusToText[status],
  onClick
}));

const StatusAwareButtonComponent = ({ color, text, onClick }) =>
  <Button color={color} size="lg" className="pl-5 pr-5 h6" onClick={onClick}>
    {text}
  </Button>;

StatusAwareButtonComponent.propTypes = {
  color: PropTypes.oneOf(['info', 'danger', 'success', 'primary']),
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

const StatusAwareButton = statusEnhance(StatusAwareButtonComponent);

StatusAwareButton.propTypes = {
  status: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

// ==================== Add to cart button component ====================
const AddToCartButton = ({ status, handleClick, quantity, handleChange }) => (
  <React.Fragment>
    <div className="col-4">
      <input type="number"
        min="1"
        className="form-control form-control-lg"
        value={quantity}
        onChange={handleChange}
      />
    </div>
    <div className="col-6 p-0">
      <StatusAwareButton status={status} onClick={handleClick} />
    </div>
  </React.Fragment>
);

AddToCartButton.propTypes = {
  status: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  quantity: PropTypes.number.isRequired
};

// ==================== Logic ====================

// This function should be edited according to
// the current project's product options / tags.
const resolveVariantId = variants => {
  const size = window.slateplus.Size;
  return find(variants, ['option1', size]).id;
};

const enhance = compose(
  connectCart,
  withState('quantity', 'setQuantity', 1),
  withHandlers({
    handleChange: ({ setQuantity }) => e => setQuantity(e.target.value),
    handleClick: ({ cart, product, quantity }) => () => (
      cart.addToCart(resolveVariantId(product.variants), quantity)
    )
  }),
  mapProps(({ handleClick, handleChange, quantity, cart }) => ({
    status: cart.state.status, handleClick, handleChange, quantity
  }))
);

export default enhance(AddToCartButton);
