import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row, Button } from 'reactstrap';
import { printPrice } from 'templates/utils';

const CartPageFooter = ({ cart, currency }) =>
  <Row>
    <Col md="5" className="mb-4 mb-md-0">
      <label htmlFor="del-note" className="d-block">DELIVERY INSTRUCTIONS</label>
      <textarea id="del-note" className="w-100 note-field px-2 py-1" rows="4" placeholder="Notes for delivery..."></textarea>
    </Col>
    <Col md="7">
      <div className="text-right">
        <h4 className="h4 mb-2">TOTAL <span className="total">{printPrice(cart.state.total_price, currency)}</span></h4>
        <div className="d-inline-block bg-secondary text-white py-1 px-2 mb-4">FREE EXPRESS SHIPPING!</div>
        <div className="d-block d-sm-flex align-items-center justify-content-end">
          <a href="#" className="d-block d-sm-inline-block mr-0 mr-sm-3 mb-3 mb-sm-0">Continue Shopping Or</a>
          <Button type="submit" color="primary" className="font-weight-bold text-white text-uppercase px-4">CONTINUE TO CHECKOUT</Button>
        </div>
      </div>
    </Col>
  </Row>;

CartPageFooter.propTypes = {
  cart: PropTypes.object.isRequired,
  currency: PropTypes.string
};

CartPageFooter.defaultProps = {
  currency: 'AUD'
};

export default CartPageFooter;
