import React from 'react';

import { Col, Row } from 'reactstrap';

const CartPageHeader = () =>
  <Row className="d-none d-md-flex">
    <Col md={{ size: 10, offset: 2 }}>
      <Row>
        <Col md={{ size: 2, offset: 5 }}>
          <div className="text-right">PRICE</div>
        </Col>
        <Col md="2">
          <div className="text-right">QUANTITY</div>
        </Col>
        <Col md="2">
          <div className="text-right">TOTAL</div>
        </Col>
      </Row>
    </Col>
  </Row>;

export default CartPageHeader;
