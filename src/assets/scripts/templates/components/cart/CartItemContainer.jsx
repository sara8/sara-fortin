import React from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'lodash-es';

const CartItemContainer = ({
  cart,
  headerComponent: HeaderComponent,
  itemComponent: ItemComponent,
  footerComponent: FooterComponent,
  debounceTimer }) => {
  const handleChange = item => debounce(quantity => cart.updateInCart(item.id, quantity), debounceTimer);
  const handleRemove = item => () => cart.removeFromCart(item.id);

  return (
    <React.Fragment>
      <section>
        { HeaderComponent &&
          <HeaderComponent cart={cart} />
        }
        { cart.state.items.length ? cart.state.items.map(item =>
          <ItemComponent
            key={item.id}
            item={item}
            currency={cart.state.currency}
            onChange={handleChange(item)}
            onRemove={handleRemove(item)}
          />)
          : <h2>YOUR CART IS EMPTY</h2>
        }
      </section>
      <section>
        { FooterComponent &&
          <FooterComponent cart={cart} />
        }
      </section>
    </React.Fragment>
  );
};

CartItemContainer.propTypes = {
  cart: PropTypes.object,
  headerComponent: PropTypes.func,
  itemComponent: PropTypes.func.isRequired,
  footerComponent: PropTypes.func,
  debounceTimer: PropTypes.number
};

CartItemContainer.defaultProps = {
  debounceTimer: 1000
};

export default CartItemContainer;
