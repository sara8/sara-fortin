import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'reactstrap';
import { printPrice } from 'templates/utils';

const CartItem = ({ item, currency, onChange, onRemove }) => {
  const doChange = e => onChange(e.target.value);
  const doRemove = () => onRemove();

  return (
    <div className="mb-4 mb-sm-2">
      <Row>
        <Col md="2" sm="3">
          <div className="mb-4 mb-sm-0 text-center text-sm-left">
            <a href={item.url} className="d-inline-block">
              <img src={item.image} className="mw-100" />
            </a>
          </div>
        </Col>
        <Col md="10" sm="9" className="d-flex align-items-md-center">
          <Row className="w-auto align-items-sm-center align-items-md-start">
            <Col md="5">
              <h3 className="h6 mb-3 mb-sm-2">
                {item.product_title}
              </h3>
              <span className="d-inline-block mb-2 mb-sm-0">{item.variant_title}</span>
            </Col>
            <Col xs="4" md="2" sm="3">
              <div className="text-right d-flex d-md-block">{printPrice(item.price, currency)}</div>
            </Col>
            <Col xs="4" md="2" sm="3">
              <div className="text-right d-flex d-md-block justify-content-center justify-content-sm-start">
                <input className="qty-field mw-100 text-center px-2 py-1" type="number" min="1" defaultValue={item.quantity} onChange={doChange} />
              </div>
            </Col>
            <Col xs="4" md="2" sm="3">
              <div className="text-right d-flex d-md-block justify-content-end justify-content-sm-start">{printPrice(item.original_line_price, currency)}</div>
            </Col>
            <Col md="1" sm="3">
              <div onClick={doRemove} style={{ cursor: 'pointer' }} className="text-right d-flex d-sm-block justify-content-end justify-content-sm-start">
                <i className="fas fa-times-circle"></i>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

CartItem.propTypes = {
  item: PropTypes.object.isRequired,
  currency: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired
};

CartItem.defaultProps = {
  currency: 'AUD'
};

export default CartItem;
