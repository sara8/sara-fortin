const loginForm = document.getElementById('CustomerLoginForm');
const resetForm = document.getElementById('RecoverPasswordForm');

const forgetPasswordLink = document.getElementById('RecoverPassword');
const cancelButton = document.getElementById('HideRecoverPasswordLink');

forgetPasswordLink.addEventListener('click', () => {
  loginForm.classList.toggle('d-none');
  resetForm.classList.remove('d-none');
});

cancelButton.addEventListener('click', () => {
  resetForm.classList.add('d-none');
  loginForm.classList.remove('d-none');
});
