const addressNewForm = document.getElementById('AddressNewForm');
const header = document.getElementById('Header');
const addressCards = document.getElementById('AddressCards');
const footerButtons = document.getElementById('FooterButtons');

const addNewAddressButton = document.getElementById('AddNewAddress');
const hideAddNewAddressForm = document.getElementById('HideAddNewAddressForm');

const editButtons = document.getElementsByClassName('edit');
const editCancelButtons = document.getElementsByClassName('edit-cancel');
const cards = document.getElementsByClassName('card');

addNewAddressButton.addEventListener('click', () => {
  addressNewForm.classList.remove('d-none');
  header.classList.add('d-none');
  addressCards.classList.add('d-none');
  footerButtons.classList.add('d-none');
});

hideAddNewAddressForm.addEventListener('click', () => {
  addressNewForm.classList.add('d-none');
  header.classList.remove('d-none');
  addressCards.classList.remove('d-none');
  footerButtons.classList.remove('d-none');
});

[].forEach.call(editButtons, (el) => {
  el.addEventListener('click', () => {
    const currentFormId = el.getAttribute('data-form-id');
    const form = document.getElementById('EditAddress_' + currentFormId);

    form.classList.remove('d-none');
    header.classList.add('d-none');
    footerButtons.classList.add('d-none');
    hideAllCards();
  });
});

[].forEach.call(editCancelButtons, (el) => {
  el.addEventListener('click', () => {
    const currentFormId = el.getAttribute('data-form-id');
    const form = document.getElementById('EditAddress_' + currentFormId);

    form.classList.add('d-none');
    header.classList.remove('d-none');
    footerButtons.classList.remove('d-none');
    showAllCards();
  });
});

function hideAllCards() {
  [].forEach.call(cards, (el) => {
    el.classList.add('d-none');
  });
}

function showAllCards() {
  [].forEach.call(cards, (el) => {
    el.classList.remove('d-none');
  });
}
