/**
 * Transforms an image url to utilize Shopify's image resizing service.
 *
 * See https://www.shopify.com/partners/blog/img-url-filter for more info.
 *
 * @param {string} url  the image url to transform
 * @param {string} size the size parameter to insert into the file name
 */
export const resizeImage = (url, size) =>
  url.replace(/\.(jpg|png)/, (_, extension) => `_${size}.${extension}`);
