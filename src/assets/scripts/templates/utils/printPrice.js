/**
 * Formats prices using the provided currency value.
 *
 * @param {number} value    the raw price value
 * @param {string} currency the currency abbreviation (e.g AUD, USD, etc...)
 */
export const printPrice = (value, currency) =>
  `${parseFloat(value / 100).toFixed(2)} ${currency}`;
