import React from 'react';
import ReactDOM from 'react-dom';

import { connectCart } from '../react-cart';

import CartItem from './components/cart/CartItem';
import CartButton from './components/cart/CartButton';
import CartPageHeader from './components/cart/CartPageHeader';
import CartPageFooter from './components/cart/CartPageFooter';
import CartItemContainer from './components/cart/CartItemContainer';

const ConnectedCartItemContainer = connectCart(CartItemContainer);

ReactDOM.render(
  <CartButton />,
  document.getElementById('cart-btn')
);

ReactDOM.render(
  <ConnectedCartItemContainer
    headerComponent={CartPageHeader}
    itemComponent={CartItem}
    footerComponent={CartPageFooter}
    debounceTimer={1500}
  />,
  document.getElementById('cart-item-container')
);
