import React from 'react';
import ReactDOM from 'react-dom';
import { InstantSearch, connectMenu } from 'react-instantsearch-dom';

import CollectionsPage from './components/collections/CollectionsPage';

const VirtualMenu = connectMenu(() => null);

const currentCollection = location.pathname.substr(location.pathname.lastIndexOf('/') + 1);

ReactDOM.render(
  <InstantSearch
    appId="XCDDGYBV0X"
    apiKey="dc6d52c800b479e398bb2beb7ccdb078"
    indexName="shopify_products"
  >
    {currentCollection !== 'all' &&
      <VirtualMenu attribute="collections" defaultRefinement={currentCollection} />
    }
    <CollectionsPage />
  </InstantSearch>,
  document.getElementById('list-collections-root')
);
