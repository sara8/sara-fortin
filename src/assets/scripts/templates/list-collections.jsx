import React from 'react';
import ReactDOM from 'react-dom';
import { InstantSearch } from 'react-instantsearch-dom';

import CollectionsPage from './components/collections/CollectionsPage';

ReactDOM.render(
  <InstantSearch
    appId="XCDDGYBV0X"
    apiKey="dc6d52c800b479e398bb2beb7ccdb078"
    indexName="shopify_products"
  >
    <CollectionsPage />
  </InstantSearch>,
  document.getElementById('list-collections-root')
);
