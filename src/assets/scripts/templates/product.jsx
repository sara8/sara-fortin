import React from 'react';
import ReactDOM from 'react-dom';

import CartButton from './components/cart/CartButton';
import AddToCartButton from './components/cart/AddToCartButton';

ReactDOM.render(
  <CartButton />,
  document.getElementById('cart-btn')
);

ReactDOM.render(
  <AddToCartButton product={window.slateplus.activeItem} />,
  document.getElementById('add-to-cart-btn')
);
