const { spawn } = require('child_process');
const { resolve } = require('path');
const { writeFileSync, existsSync, unlinkSync } = require('fs');

class ThemeKitPlugin {
  constructor(options) {
    this.options = {
      executable: options.executable || 'theme',
      command: options.command || 'watch',
      args: options.args || [],
      cwd: options.cwd || resolve(__dirname, '../'),
      notifyFile: options.notifyFile || resolve(__dirname, '../.notify')
    };
    this.instance = null;
    writeFileSync(this.options.notifyFile, '');
    process.on('exit', () => {
      if (existsSync(this.options.notifyFile)) {
        unlinkSync(this.options.notifyFile);
      }
    });
  }

  spawnThemeKit() {
    if (!this.instance) {
      const { executable, command, args, cwd } = this.options;
      if (command === 'watch') {
        this.instance = spawn(executable, ['deploy', '--nodelete', ...args], { cwd, stdio: 'inherit' });
        this.instance.on('exit', () => this.instance = spawn(executable, [command, '-n', this.options.notifyFile, ...args], { cwd, stdio: 'inherit' }));
      } else {
        this.instance = spawn(executable, [command, ...args], { cwd, stdio: 'inherit' });
      }
    }
  }

  apply(compiler) {
    compiler.hooks.done.tap('themekit_start', () => {
      this.spawnThemeKit();
    });

    compiler.hooks.watchClose.tap('themekit_stop', () => {
      if (this.instance) {
        this.instance.kill();
      }
    });
  }
}

module.exports = ThemeKitPlugin;
