const browserSync = require('browser-sync').create();
const debounce = require('lodash/debounce');

const { resolve } = require('path');
const { watch } = require('fs');

class BrowserSyncPlugin {
  constructor(url) {
    this.url = url;
    this.notifyFile = resolve(__dirname, '../.notify');
  }

  spawnBrowserSync() {
    browserSync.init({
      https: true,
      proxy: this.url,
      injectChanges: false
    });
  }

  apply(compiler) {
    this.spawnBrowserSync();

    watch(this.notifyFile, debounce(() => browserSync.reload(), 100));

    compiler.hooks.watchClose.tap('browsersync_stop', () => {
      browserSync.exit();
    });
  }
}

module.exports = BrowserSyncPlugin;
