const fs = require('fs');
const chalk = require('chalk');

const customerTemplates = [
  'account',
  'activate_account',
  'addresses',
  'login',
  'order',
  'register',
  'reset_password'
];

function templateSnippet(name) {
  const templateName = customerTemplates.includes(name) ? `customers/${name}` : name;

  return `
{%- if template == '${templateName}' -%}
  {{ '${name}.template.js' | asset_url | script_tag }}
{%- endif -%}
`;
}

class ScriptTagsSnippetPlugin {
  constructor({ entries, snippetsFolder }) {
    this.options = {
      entries,
      snippetsFolder: snippetsFolder || 'dist/snippets'
    };
    this.generated = false;
  }

  apply(compiler) {
    compiler.hooks.done.tap('script-tags-snippet', stats => {
      if (stats.compilation.errors.length) {
        return;
      }
      if (!this.generated) {
        this.generated = true;
        const { entries, snippetsFolder } = this.options;

        const grouped = Object.keys(entries)
          .filter(entry => entry.includes('template'))
          .map(entry => entry.substring(0, entry.indexOf('.template')))
          .map(templateSnippet)
          .join('');

        if (!fs.existsSync(snippetsFolder)) {
          fs.mkdirSync(snippetsFolder);
        }

        fs.writeFileSync(`${snippetsFolder}/script-tags.liquid`, grouped);

        // eslint-disable-next-line
        console.log(`\n${chalk.green('[ScriptTagsSnippetPlugin]:')} ${chalk.cyan('script-tags')} snippet has been successfully generated!`);
      }
    });
  }
}

module.exports = ScriptTagsSnippetPlugin;
