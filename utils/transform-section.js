const fs = require('fs');
const path = require('path');

const JS_OPENING_TAG = '{% javascript %}';
const JS_CLOSING_TAG = '{% endjavascript %}';
const assetsDir = path.resolve(__dirname, '..', 'dist', 'assets');
const sectionsDir = path.resolve(__dirname, '..', 'dist', 'sections');

class TransformSectionsPlugin {
  apply(compiler) {
    compiler.hooks.done.tap('transform-sections-plugin', stats => {
      if (stats.compilation.errors.length) {
        return;
      }
      fs.readdirSync(assetsDir)
        .filter(script => script.endsWith('.section.js'))
        .map(script => ({
          section: `${script.substring(0, script.indexOf('.'))}.liquid`,
          script
        }))
        .forEach(({ section, script }) => {
          const sectionFile = path.join(sectionsDir, section);
          const scriptFile = path.join(assetsDir, script);

          let sectionSource = fs.readFileSync(sectionFile).toString();

          if (sectionSource.includes(JS_OPENING_TAG)) {
            sectionSource = sectionSource.split(JS_OPENING_TAG)[0];
          }

          fs.writeFileSync(sectionFile, [
            sectionSource,
            JS_OPENING_TAG,
            fs.readFileSync(scriptFile),
            JS_CLOSING_TAG].join('\n')
          );
        });
    });
  }
}

module.exports = TransformSectionsPlugin;
